#include <iostream>
using namespace std;

#include "TGraph.h"
#include "TThread.h"

#include "LS7336_pulseCntrDAQ.h"
#include "LS7336.h"

//========================
//========================
LS7336_pulseCntrDAQ::LS7336_pulseCntrDAQ(const char *core) {
  mCore=core;
  printf("constr of LS7336_pulseCntrDAQ=%s\n",mCore.Data());

  //  int clk_hz=2*512*1024;
  int clk_hz=900*1024;
  int spiChan=0;
  pulseCntr=new LS7336 (spiChan,clk_hz);

  pulseCntr->setCounterDepth(LS7336::cycleX4, LS7336::twoByte);
  pulseCntr->readRegisters(); // for x-check
  pulseCntr->reset(0);  // 0-force zero offset regardless of current position
  pulseCntr->debug=0;

  int offset= pulseCntr->getCntrOffset();
  printf("ZERO counter position=%d  of %d\n",offset,pulseCntr->maxCntr() );
  
  TThread::Lock(); //Static method to lock the main thread mutex.
  TGraph *gr;

  outGr1=gr=new TGraph(); // position
  gr->SetLineColor(2);
  gr->SetLineWidth(2);
  gr->SetMarkerColor(kBlue);
  gr->SetMarkerStyle(4); // open circle


  outGr2=gr=new TGraph(); // speed
  gr->SetLineColor(8);
  gr->SetLineWidth(2);
  gr->SetMarkerColor(46);
  gr->SetMarkerStyle(27); // open diamond

  outGr3=gr=new TGraph(); // phase diagram
  gr->SetLineColor(9);
  gr->SetLineWidth(1);
  gr->SetMarkerColor(6);
  gr->SetMarkerSize(0.2);
  gr->SetMarkerStyle(4); // open circle

  lastT=0; lastPos=0; ; lastVel=0;
  TThread::UnLock();   
}

//======================== 
//========================
void LS7336_pulseCntrDAQ::addPoint(){
  double par_alpha=0.8; // weight of current vel-measurement

  int rotPos=pulseCntr->position();
  double myTime=pulseCntr->procTime();

  double delPos=rotPos-lastPos; lastPos=rotPos;
  double delT=myTime-lastT; lastT=myTime;
  double rotVel=lastVel*(1.-par_alpha);  
  if(delT>0) rotVel+=par_alpha*delPos/delT;
  lastVel=rotVel;

  TThread::Lock(); 
  int n=outGr1->GetN();
  outGr1->SetPoint(n,myTime,rotPos);
  outGr2->SetPoint(n,myTime,rotVel/3.);
  outGr3->SetPoint(n,rotPos,rotVel/3.);
  TThread::UnLock();
  // printf("n=%d, pos=%d  vel=%.1f time=%.4f sec\n",n,rotPos, rotVel,myTime);
}
//========================
//========================
void LS7336_pulseCntrDAQ::print() {
}


//========================
//========================
void LS7336_pulseCntrDAQ::init() {

}


//________________________________________________
//________________________________________________
void
LS7336_pulseCntrDAQ::initHistos(){
 
}


//________________________________________________
//________________________________________________
void
LS7336_pulseCntrDAQ::finish(){

  printf("::::::::::::::::LS7336_pulseCntrDAQ::finish(%s) :::::::::::\n",mCore.Data());
  
}

