#include <assert.h>
#include <TString.h> 

class LS7336;
class TGraph;

//old
class TH1;


class LS7336_pulseCntrDAQ {
 private:
  LS7336 *pulseCntr;
  TString mCore;

  double lastT, lastPos, lastVel;

  // histograms
  enum {mxHA=128}; TH1 * hA[mxHA];
  void initHistos();

 public:
  LS7336_pulseCntrDAQ(const char *core); 
  TGraph *outGr1, *outGr2, *outGr3;
  void addPoint();
  void print();
  void init();
  void finish();
  

};

