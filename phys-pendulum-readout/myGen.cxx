#include <cstdio>  // C++ standard since 1999
#include <TObjArray.h>
#include <TFile.h>
#include <TLegend.h>

#include "LS7336_pulseCntrDAQ.h"


#include <TApplication.h>
#include "TCanvas.h"
#include "TThread.h"
#include "TFrame.h"
#include "TGraph.h"
#include "TH1.h"
#include "TStyle.h"


TCanvas *can=0;
TThread *trdFlr=0, *trdPlt=0;

int fillPeriod_ms=20;
int plotPeriod_ms=2000;
long fillDuration_s=20;
int plType=2; // 1= vs. time, 2=phase diagr

void *handleFiller(void *ptr) {
   long nSec = (long) ptr;
   printf("F:start Filler inp=%ld\n",nSec);

   LS7336_pulseCntrDAQ  daq("pulseCntrDaq");

   int j=0;
   while (can==0) {
     printf("F:wait for Canvas j=%d\n",j);
     j++;
     gSystem->Sleep(fillPeriod_ms); // mSec 
   }


   printf("F:plot 1st time, plType=%d\n",plType);
   can->cd(1);     
   TLegend* leg = new TLegend(0.8,0.8,0.99,0.99);
   leg->SetHeader("My Pendulum");

   if(plType==1) {
     daq.outGr1->Draw("pl");
     daq.outGr2->Draw("pl");
     
     leg->AddEntry(daq.outGr1,"ang pos","lp");
     leg->AddEntry(daq.outGr2,"ang vel","lp");
   }

   if(plType==2) {
     daq.outGr3->Draw("pl");
     leg->AddEntry(daq.outGr3,"pos-vel","lp");
   }


   leg->Draw();


   int time0=time(0);
   int lastTime=time0; 
   int nowTime=time0;
   // if(can) can->Modified();
   printf("F:add data \n");
   while ( nowTime <time0+nSec) {
     daq.addPoint();
     TThread::Lock(); 
     if(can) can->Modified();
     TThread::UnLock();
     gSystem->Sleep(fillPeriod_ms); // mSec  , note 100Hz works almost smooth w/o WiFi
     if (nowTime >lastTime+2) {
       int n= daq.outGr1->GetN();
       double tim,pos,vel;
       daq.outGr1->GetPoint(n-1,tim,pos);
       daq.outGr2->GetPoint(n-1,tim,vel);
       printf("F: recorded %d samples, time/sec=%d last pos=%.1f speed=%.1f\n", daq.outGr1->GetN(),nowTime-time0,pos,vel);  
       lastTime=nowTime;
     }
     nowTime=time(0);
   }
   printf("F: done, size=%d\n", daq.outGr1->GetN());
   
   return 0;
}



//================================
//================================
void *handlePlotter(void *ptr) {
  long val=(long) ptr;
  printf("P:start Plotter inp=%ld\n",val);
  
  TString tit1=Form("can0");
  TString tit2=Form("Dynamic Filling TGraph");
  can = new TCanvas(tit1,tit2,100,20,800,800);
   can->Divide(1,1);
   can->cd(1); 

  if(plType==1) {
    TH1F *hP=new TH1F("aa","rotary counter vs time; time(sec); pulseCnt",100,0.,fillDuration_s);
    hP->SetMinimum(-300);   hP->SetMaximum(300);
    hP->Draw(); 
  }
  if(plType==2) {
    float mxX=400;
    TH1F *hP=new TH1F("bb","phase diagram; position (cnt); speed (#Delta cnt/#Delta time)",100,-mxX,mxX);
    hP->SetMinimum(-mxX);   hP->SetMaximum(mxX);
    hP->Draw(); 
  }



   gPad->SetGrid();
   gStyle->SetOptStat(0);
  
  // connect to the Closed() signal to kill thread when a canvas is closed
   // can->Connect("Closed()", 0, 0, "killMyThr()");



  printf("P: infinite loop updating plot ... is disabled\n");
  while (can ) {

       if ( can->IsModified()) {
	 // printf("P:Update canvas\n" );
	 //TThread::Lock(); 
	 //x can->Update();
	 //TThread::UnLock();
       }

       gSystem->Sleep(plotPeriod_ms); // needed to not quit this thread
       // gSystem->ProcessEvents();
  }  
  return 0;
}



  
int main(int argc, char* argv[]) {
  printf("myGenerator:  START...\n");

  //gDebug = 1; // prints what is load to Root
  
  TApplication app ("app",&argc,argv);
  
  printf("M: TApp started\n");
  

  TString tit4=Form("myPltTrd");
  printf("M:Starting Thread %s\n",tit4.Data());
  trdPlt = new TThread(tit4, handlePlotter, (void*) 44);
  trdPlt->Run();
  gSystem->Sleep(1000); // mSec 

  TString tit3=Form("myFlrTrd");
  printf("M:Starting Thread %s\n",tit3.Data());
  trdFlr = new TThread(tit3, handleFiller, (void*) fillDuration_s); // Note, it is used to index canvas - do not change it
  trdFlr->Run();
  
  
  
  TThread::Ps();
  printf("M:wait for fill-thread to finish...\n");
  trdFlr->Join(); // wait untill thread is cancelled
  printf("M:fill-done, wait for plt-thr to finish ...\n");
  
  can->Update();
  
  trdPlt->Join(); 
  printf("M:plot-done, wiat for plt-thr to finish ...\n");
  TThread::Ps();
  
  delete trdPlt; 
  delete trdFlr;
  
  printf("M: all threads are terminated, quit Root w/ .q\n");  
  return 0;
}

