% model neuron,
clear all

global w; % strength of coupling

% w range (0,big), bifurcation starts at w=1
'draw stat space for w=3'
w=3;
x=[-2:.1:2];
xdot=d_x(x);
figure(1);
plot(x,xdot);
grid on;
xlabel('x ');
ylabel('$\dot{x}$','interpreter','latex');
title('textbook, chapter 2, neuron model');

xz=zd_x(); % finds all 3 zeros
yz=d_x(xz);

hold on
plot(xz,yz,'or');
hold  off


'draw dependence of stability points on w'

wL=[0.1 : 0.1:2.5];
x1=wL; x2=wL; x3=wL; % init output vectors
dim=size(wL);

for i=1:dim(2)
    w=wL(i);
    out=zd_x();
    x1(i)=out(1);
    x2(i)=out(2);
    x3(i)=out(3);
end



figure(2);
plot(wL,x1,'ro-');
hold on
plot(wL,x2,'bo--');
plot(wL,x3,'go-');
hold off
legend('stable1','unstable','stable2');
grid on;
xlabel('w ');
ylabel('x*');
title('bifurcation of stability points');
axis([0 2.5 -1.2 1.2]);
