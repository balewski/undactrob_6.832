function xdot  = d_x(  x )
% model of neuron
global w; % strength of coupling
xdot=-x +tanh(x.*w);
end

