function [ L ] = zd_x(  )
% find all 3 zeros
xi=[-1,0,1];
xz=xi; % initialize array
for i=1:3    
    xz(i)=fzero(@d_x,xi(i));
end
L=xz;

end

