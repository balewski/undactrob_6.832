prog = spotsosprog;
[prog, a] = prog.newFree(1);
P = [2, a, 1; a,5,0; 1,0,-1-2*a];
prog = prog.withPSD(P);
sol = prog.minimize(a, @spot_mosek);
a = double(sol.eval(a))
