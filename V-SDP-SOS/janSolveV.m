function solveV
clear all
% Use SOS to find a Lyapunov function to certify the global stability of
% the damped pendulum.  Uses trig to poly substitution to perform exact
% analysis.  Recovers a Lyapunov function that looks a lot like the energy,
% but is better!  (doesn't require LaSalle's theorem)

checkDependency('spotless');
checkDependency('mosek');

% set up state variables
xone = msspoly('xone',1);
xtwo = msspoly('xtwo',1);
x = [xone;xtwo];

% polynomial dynamics
f=[ xtwo -xone^3; -xtwo^3 -xone];
x0 = [0;0];
eps = 1e-4;

prog = spotsosprog;
prog = prog.withIndeterminate(x);

deg_V = 3;
[prog,V] = prog.newFreePoly(monomials(x,0:deg_V));
prog = prog.withSOS(V - eps*(x-x0)'*(x-x0));  % V is strictly positive away from x0
Vdot = diff(V,x)*f; 


prog = prog.withEqs( subs(V,x,x0) );  % V(0) = 0

solver = @spot_mosek;
%solver = @spot_sedumi;
options = spot_sdp_default_options();
options.verbose = 1;

sol = prog.minimize(0,solver,options);

global V Vdot x;
V = sol.eval(V)  % this is scalar
Vdot = sol.eval(Vdot); % this is vector with 2 components?

MX=3; DX=0.5;
[X1,X2]=meshgrid(-MX:DX:MX, -MX:DX:MX);  % for surfs


figure(1);
subplot(1,2,1);
ezcontour(@(x, y) msubs(V, [xone; xtwo], [x; y]), [-MX,MX,-MX,MX]);
title('$$ V $$','interpreter','latex','fontsize',20) 
xlabel('$$ x_1 $$','interpreter','latex','fontsize',15)
ylabel('$$ x_2 $$','interpreter','latex','fontsize',15)
subplot(1,2,2);
%Vmesh = dmsubs(V,x,[sin(Theta(:)');cos(Theta(:)');ThetaDot(:)']);
Vmesh = dmsubs(V,x,[X1(:)';X2(:)']);
%surf(Theta,ThetaDot,reshape(Vmesh,size(Theta)));
surf(X1,X2,reshape(Vmesh,size(X1)));
title('$$ V $$','interpreter','latex','fontsize',20) 
xlabel('$$ x_1 $$','interpreter','latex','fontsize',15)
ylabel('$$ x_2 $$','interpreter','latex','fontsize',15)


end
