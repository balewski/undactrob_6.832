alpha = .85;
k = randi(100);
R = diag(rand(2,1));
B = randn(4,2);
A = randn(4);
S_k1 = 100*diag(rand(4,1));

M = A'*S_k1*B*inv(alpha^k*R + B'*S_k1*B)*B'*S_k1*A;