alpha = .85;
N = 10;
A = [2 1.5; -3 2];
B = [0;1];
Q = diag([5;1]);
R = 1;
S_N = 10*eye(2);

S_k1=S_N;
for i=1:N
  k=N-i;
  M = A'*S_k1*B*inv(alpha^k*R + B'*S_k1*B)*B'*S_k1*A;
  S_k=alpha^k *Q - M + A'*S_k1*A;
  S_k1=S_k;
 
end
S_0 =S_k 

