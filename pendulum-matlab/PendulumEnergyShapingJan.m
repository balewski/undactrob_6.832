classdef PendulumEnergyShapingJan < DrakeSystem
  
  properties 
    p
  end
  
  methods
    function obj = PendulumEnergyShapingJan(plant)
      obj = obj@DrakeSystem(0,0,2,1,true,true);
      typecheck(plant,'PendulumPlantJan');
      obj.p = plant;
      obj = setInputFrame(obj,PendulumState);
      obj = setOutputFrame(obj,PendulumInput);
    end
    
    function y = output(obj,t,~,u)
      Etilde = .5*obj.p.m*obj.p.l^2*u(2)^2 - obj.p.m*obj.p.g*obj.p.l*cos(u(1)) - 1.1*obj.p.m*obj.p.g*obj.p.l;
      y = +obj.p.b*u(2)-.01*u(2)*Etilde;
    end
  end 

  methods (Static)
    function run()  
      pd = PendulumPlantJan;
      %pd = setInputLimits(pd,-inf,inf);
      maxTorque=0.5;
      pd = setInputLimits(pd,-maxTorque, maxTorque);
      pv = PendulumVisualizer();
      c = PendulumEnergyShapingJan(pd);

      sys = feedback(pd,c);
      
      
      %j xtraj = simulate(sys,[0 6]);
      %j playback(pv,xtraj);
      %j figure(2); fnplt(xtraj);
      
      
      end_time=25
     
      [ytraj,xtraj] = simulate(sys,[0 end_time]);
      
      playback(pv,xtraj);
      figure(2); fnplt(xtraj);  grid on;
      title('Pendulum phase portrait');
      xlabel('$\theta$ (rad)','interpreter','latex') ; 
      ylabel('$\dot{\theta}$ (rad/sec)','interpreter','latex');  
   
      tau=[]; % torque
      P=[]; % driving power
      for t=0:0.1:end_time
          q = xtraj.eval(t);
          y=c.output(0,0,q);  
          tau = [tau,[y;t]];
          P = [P,[y*q(2);t]];
          %[T,U]=c.p.energy([q(1),q(2)]) ; % kinetic+potential
      end
      figure(3);       plot(tau(2,:),tau(1,:));
      grid on; xlabel('time (sec)'); ylabel('torque (N*m)');
      
      figure(4);       plot(P(2,:),P(1,:))
      grid on; 
      xlabel('time (sec)'); ylabel('$\tau *\dot{\theta}$ (W)','interpreter','latex');  
       
           
    end
  end  
end
