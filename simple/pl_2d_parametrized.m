clear all
checkDependency('spotless');
checkDependency('mosek');

% set up state variables
xone = msspoly('xone',1);
xtwo = msspoly('xtwo',1);
%x = [xone;xtwo];

% solution to Pset removed on purpose

%plot countur of a function
V=[  (2.1)*xone^3 - (0.68)*xtwo^2  ];
ezcontour(@(x, y) msubs(V, [xone; xtwo], [x; y]), [-2.1, -3.2])
%BAD: ezcontour(@(xone,xtwo)V(xone,xtwo), [-2,1,-3,2]); 

