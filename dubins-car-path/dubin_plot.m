theta=pi/2;
r=3; % magnitued of arrow
x=4; % begin of arrow
y=5; 
u=r*cos(theta); % arrow vector
v=r*sin(theta);
h=quiver(x,y,u,v);
set(gca,'XLim',[1 10],'YLim',[1 10]);