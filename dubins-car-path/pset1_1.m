clear all
' cost function for Dubins car'

Qf=[0 0 0 ;    0 0 0 ;    0 0 1 ]

'xy  to uv '
M=[1 0 0; -1 2 0; 0 0 1]
MT=M'
Qn=[0 0 0 ; 0 1 0 ; 0 0 0]
Qf=M'*Qn*M

'try 1'
X=[ 0 0 7]'
Qx=X'*Qf*X

'try 2'
X=[ 2 1 3]'
Qx=X'*Qf*X

'try 3'
X=[ 4 2 -4]'
Qx=X'*Qf*X