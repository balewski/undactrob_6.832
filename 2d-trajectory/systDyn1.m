function dx=systDyn1(t,x)
   dx = zeros(2,1);
   aa=1+x(1)*x(1);
   dx(1) = -6*x(1)/aa/aa+2*x(2);    % your x1' here
   dx(2) = -2*(x(1)+x(2))/aa/aa;     % your x2' here
end
