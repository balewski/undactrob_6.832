nx = 20;
ny = 20;
intvalx = linspace(-10,10,nx);
intvaly = linspace(-10,10,ny)
figure;hold on;
for ii=1:nx,
    for jj=1:ny,
        [t,x] = ode45(@systDyn1,[0 1],[intvalx(ii) intvaly(jj)]);
        plot(x(:,1),x(:,2));
    end
end
