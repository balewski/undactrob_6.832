#include <stdio.h>
#include "TCanvas.h"

#include "TString.h"
#include "TH1.h"
#include "TMath.h"
#include "TThread.h"
#include "TStyle.h"
#include "TLegend.h"

#include "LiveData.h"
#include "sharedObj.h"


void *handlePlotterB(void *ptr) {
  long yMax=(long) ptr;
  printf("PB:start Plotter yMax=%ld\n",yMax);

  TLegend* leg1 = new TLegend(0.8,0.8,0.99,0.99);
  leg1->SetHeader("Pendulum vs. time");
  leg1->AddEntry(liveData1->getGr(),"ang pos","lp");
  leg1->AddEntry(liveData2->getGr(),"ang vel/5","lp");
 
  TString tit1=Form("can0");
  TString tit2=Form("Filling vs. time B");
  TCanvas *can = new TCanvas(tit1,tit2,50,20,700,500);

  printf("PB:draw Canvas\n");
  int nUpd=0; // update counter
  int nData=0;

  TH1F *hP=new TH1F("hP"," updated by a thread;time(sec); amplitude (a.u.)",1000,0.,fillDuration_s);
  
  hP->SetMinimum(-yMax);   hP->SetMaximum(yMax);
  hP->SetAxisRange(0,displayDuration_s);
    
  can->cd(0);
  hP->Draw();
  leg1->Draw();
  gPad->SetGrid();
  gStyle->SetOptStat(0);

  printf("PB:about to plot 1st time\n");
  int time0=time(0); // to slide time window
  
  printf("PB: infinite loop updating plot ...\n");
  while (1) {
    LiveData *lastData1=0; 
    LiveData *lastData2=0; 
        
    TThread::Lock(); //Static method to lock the main thread mutex.
    int np=liveData1->getN();
    //printf("PB:sees %d live data\n",np);
    if (np >1 ) {
      lastData1=liveData1; // archive old instance for plotting
      liveData1=lastData1->newChunk();
      
      lastData2=liveData2; 
      liveData2=lastData2->newChunk();      
    } 
    TThread::UnLock();

    nData+=np-1;
    if (lastData1) {
      int time1=time(0)-time0;
      if(time1>displayDuration_s){ // move time plotted window
	hP->SetAxisRange(time1-displayDuration_s,time1);
      }   

      lastData1->updateVsTime(hP); // & rescale Y-axis
      lastData2->getGr()->Draw("pl");
 
      hP->SetTitle(Form(" nData=%d update=%d  period=%.1f sec\n",nData,nUpd,plotPeriod_ms/1000.));
      nUpd++;      
      //printf("P:Update canvas time1=%d  valL=%.1f \n",time1,valL );
      can->Update();
    }
    gSystem->Sleep(plotPeriod_ms);
  }

  
  return 0;
}

