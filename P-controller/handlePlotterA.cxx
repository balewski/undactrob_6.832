#include <stdio.h>
#include "TCanvas.h"
#include "TString.h"
#include "TH1.h"
#include "TMath.h"
#include "TThread.h"
#include "TStyle.h"
#include "TLegend.h"

#include "LiveData.h"
#include "sharedObj.h"

//================================
//================================
void *handlePlotterA(void *ptr) {
  long yMax=(long) ptr;
  printf("PA:start PlotterA iii=%ld\n",yMax);
 
  TLegend* legA = new TLegend(0.7,0.9,0.99,0.99);
  legA->SetHeader("Pendulum phase image");
  legA->AddEntry(liveData3->getGr(),"vel-pos","lp");
 
  TString tit11=Form("can2");
  TString tit22=Form("phase imageA");
  TCanvas *can2 = new TCanvas(tit11,tit22,500,20,700,700);

  printf("PA:draw Canvas\n");
  int nUpd=0; // update counter
  int nData=0;

  float pos1=1720, pos2=1880, velMx=100; // zoom-in up position
  //float pos1=-1500, pos2=2100, velMx=1000; // zoom-in up position
  TH1F *hP2=new TH1F("bb","phase diagram; position (cnt); speed (#Delta cnt/#Delta time)",100,pos1,pos2);
  hP2->SetMinimum(-velMx);   hP2->SetMaximum(velMx);

  can2->cd(0);
  hP2->Draw();
  legA->Draw();
  gPad->SetGrid();
  gStyle->SetOptStat(0);

   
  printf("PA: infinite loop updating plot ...\n");
  while (1) {
    LiveData *lastData3=0; 
     
    TThread::Lock(); //Static method to lock the main thread mutex.
    int np=liveData3->getN();
    //printf("P:sees %d live data\n",np);
    if (np >1 ) {
      lastData3=liveData3; 
      liveData3=lastData3->newChunk();      
    } 
    TThread::UnLock();

    nData+=np-1;
    if (lastData3) {
 
      hP2->SetTitle(Form(" nData=%d update=%d  period=%.1f sec\n",nData,nUpd,plotPeriod_ms/1000.));
      nUpd++;      
      //printf("P:Update canvas time1=%d  valL=%.1f \n",time1,valL );
      lastData3->getGr()->Draw("pl");
      can2->Update();
    }
    gSystem->Sleep(plotPeriod_ms);
  
  }

  
  return 0;
}

