/*
The data container is chopped in to small pices (LiveData.h)
and plotter adds to display only the most recent chunk.
For each data chunk  replicate last point for continues plot-line.
Note, this code creates memory leak, because objects created by:
  liveData=new LiveData(x,y);
are never deleted

Lanuches:
-  1 thread   generating record vs. time, adding to TGraph
-  1 thread updates canvas untill joiner tells it to stop
-   main thread awaiting for filller thread to finish

*/

#include <stdio.h> 
#include <TMath.h>
#include <TApplication.h>
#include "TThread.h"
#include "TGraph.h"

#include "LS7336.h" // direct access to pulserCounter chip via SPI
#include "PCA9685_motorHat.h"
#include "LiveData.h"
#include "sharedObj.h"

TThread *trdFlr=0, *trdPltA=0, *trdPltB=0;

LiveData *liveData1=0; // shared between threads
LiveData *liveData2=0; // shared between threads
LiveData *liveData3=0; // shared between threads

//----- controlling parameters
int fillPeriod_ms=20;
int plotPeriod_ms=1000;
long fillDuration_s=14;  // free fall needs 40 seconds
int displayDuration_s=10; // length of displayed time axis 
long xyRange=350; // for phase diagram
 
//===================================
//===================================
void *handleFiller(void *ptr) {
  long nSec = (long) ptr;
  printf("F:start Filler inp=%ld\n",nSec);
  
  int clk_hz=900*1024;
  int spiChan=0;
  LS7336 *pulseCntr=new LS7336 (spiChan,clk_hz);
  pulseCntr->setCounterDepth(LS7336::cycleX4, LS7336::twoByte);
  pulseCntr->readRegisters(); // for x-check
  pulseCntr->reset(1446);  // force this offset regardless of current position
  pulseCntr->debug=0;
  
  int offset= pulseCntr->getCntrOffset();
  printf("F:ZERO counter position=%d  of %d\n",offset,pulseCntr->maxCntr() );


  printf("F:DCMotor connecting ...\n");
  // Create the motor shield object with the default I2C address
  PCA9685_motorHat AFMS = PCA9685_motorHat((uint8_t)0x60);
  AFMS.begin(1600);  // create with the default frequency 1.6KHz
  // Select which 'port' M1, M2, M3 or M4. In this case, M1
  Adafruit_DCMotor *myMotor = AFMS.getMotor(1);

  
  int time0=time(0);
  int nowTime=time0;

  double par_facV=0.2; // velocity scale factor
  
  printf("F: read pulseCounter data  & drive DCmotor... \n");
  double lastTime=0,lastPos=0,lastVel=0;

  while ( nowTime <time0+nSec ) {
    double curTime=pulseCntr->procTime();
    double curPos=pulseCntr->position();
    double delT=curTime-lastTime;
    double newVel=0;

    if (delT>0) newVel=par_facV*(curPos-lastPos)/delT;
    double vel=0.4*lastVel+0.6*newVel;
     
    // PD-controller start....
    float pos0=1805;
    
    // compute torque [0,1]
    float epsPos=curPos-pos0;
    float torque=epsPos;

    int motorPwm=(int)torque;
    if (motorPwm>250) motorPwm=250;

    if( nowTime <time0+nSec-3 ) { // quit motor controll for last few seconds
    if (torque>0)  
      myMotor->run(FORWARD);
    else
      myMotor->run(BACKWARD);
    myMotor->setSpeed(motorPwm);
    } else 
      myMotor->run(RELEASE);
    // turn on motor
    
    if( nowTime <time0+3 ) { // print data at the beginning
      printf("F: new pos=%.1f  epsPos=%.1f  torque=%.1f pwm=%d\n",curPos,epsPos,torque,motorPwm);
    }
      //PD-controller end


    lastTime=curTime;
    lastPos=curPos;
    lastVel=newVel;

    TThread::Lock();     
    liveData1->addPoint(curTime, curPos);
    liveData2->addPoint(curTime,vel);
    liveData3->addPoint(curPos,vel);
    TThread::UnLock();

    gSystem->Sleep(fillPeriod_ms); // mSec 
    nowTime=time(0);
  }
  printf("F: done, \n");
  myMotor->run(RELEASE);
  return 0;
}


//================================
//================================
void *handleCloser(void *ptr) {
  long yMax=(long) ptr;
  printf("C:start closer  inp=%ld\n",yMax);
 
   TThread::Ps();
   printf("C:wait for fill-thread to finish...\n");

   if(trdFlr) {
     trdFlr->Join(); // wait untill thread is cancelled
     printf("C:fill-done, wait for plt-thr to finish ...\n");
     TThread::Ps();
     delete trdFlr; 
   }

   if(trdPltA) {
     trdPltA->Join(); 
     printf("C:plotA-done ...\n");
     TThread::Ps();
     delete trdPltA; 
   }

   if(trdPltB) {
     trdPltB->Join(); 
     printf("C:plotB-done ...\n");
     TThread::Ps();
     delete trdPltB; 
   }
   printf("C:stopped all threads\n");
  return 0; 
}


//============================================
//============================================
  
int main(int argc, char* argv[]) {

  printf("my pendulum DP-controller   START...\n");
  
  //gDebug = 1; // prints what is load to Root
  
  TApplication app ("app",&argc,argv);
  
  // initialize data container for the 1st time
  liveData1=new LiveData(0,0);  

  liveData2=new LiveData(0,0);
  liveData2->setColor(4);

  liveData3=new LiveData(0,0);
  liveData3->setColor(2);

  //gDebug = 1; // prints what is load to Root

  printf("M:Starting Thread ...\n");

#if 1
  trdPltA = new TThread("plotterA", handlePlotterA, (void*)  77);
  trdPltA->Run();
#endif

#if 0
  trdPltB = new TThread("plotterB", handlePlotterB, (void*)  77);
  trdPltB->Run();  
#endif

  trdFlr = new TThread("filler", handleFiller, (void*) fillDuration_s); 
  trdFlr->Run();
  
  
  printf("M: all threads are  dispatched\n");
  gSystem->Sleep(2*1000); // sometime code crashes instantly - may be this will help?

  handleCloser((void*)999); // will get stuck here untill all threds finish
  
  
  printf("M: all threads are terminated, quit Root w/ .q\n");  
  
  
  return 0;
}

