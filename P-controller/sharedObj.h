extern LiveData *liveData1;
extern LiveData *liveData2;
extern LiveData *liveData3;

extern int plotPeriod_ms;
extern int displayDuration_s;
extern long fillDuration_s;

void *handlePlotterA(void *ptr);
void *handlePlotterB(void *ptr);
