function [ qo ] = dd_pendulum( t ,qi)
% 2st order diff eq
% is converted to a set of 2 1st order diff eq
% qi is vector, qo is vector

global ml2 gl;

% unpack input vector
x1=qi(1);
x2=qi(2);

% set of diff eq

x1dot=x2;
x2dot= -b/ml2*x2 + u/ml2 - g2l*sin(x1);

% repack the output , make it a column
qo = [ x1dot, x2dot ];

end

