function [ xy ] = rth2xy( rth )
% Converts radial to carthesian coordinates
r=rth(1);
th=rth(2);
xy(1)=r*sin(th); %y
xy(2)=r*cos(th); %x

end

