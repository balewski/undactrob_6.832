function [ Drth ] = d_rth( t ,rth)
% 2st order diff eq
% is converted to a set of 2 1st order diff eq
% qi is vector, qddot is vector

% unpack input vector
r= rth(1);
th=rth(2);

% set of diff eq
rdot=r*(1-r);
thdot=sin(th/2)^2;


% repack the output , make it a column
Drth = [ rdot thdot ]';

end

