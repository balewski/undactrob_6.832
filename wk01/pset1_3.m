clear all

x=[-4:.1:4];
p=[1 2 -5 -6];
xdot=polyval(p,x);
eq_points = roots(p)
figure(1);
plot(x,xdot);
grid on;
xlabel('x ');
ylabel('$\dot{x}$','interpreter','latex');
title('Pset1-3  1st order dynamic system'); 

%axis([ -4 4 -10 10 ]);
'check zeros of poly'
polyval(p,eq_points)

% find  range of ..'
q=polyder(p);
'attraction range'
rr=roots(q)
zz=polyval(p,rr);
hold on
plot(rr,zz,'ro');
hold off

% extra plot
figure(2);
plot(x,polyval(q,x));
hold on
plot(rr,[0 0],'ro');
hold off

grid on;
xlabel('x ');
ylabel('$d\dot{x}/dx$','interpreter','latex');
title('Pset1-3  velocity vs. x'); 
