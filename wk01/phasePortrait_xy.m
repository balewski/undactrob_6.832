function phasePortrait_xy(hFig)

figure(hFig);
np=60;
[x,y] = ndgrid(linspace(-1.3,1.3,np),linspace(-1.3,1.3,np));

'grid constructed'

xdot=x; ydot=y;

'dims of x'
size(x)
t=0;
for i=1:prod(size(x))
  rth=xy2rth( [x(i) y(i) ]);  
  Drth=d_rth(t, [ rth(1) rth(2)]); 
  xydot=rth2xy( [Drth(1) Drth(2) ]);  
  xdot(i)=xydot(1);
  ydot(i)=xydot(2);
end

%rdot
%thdot
quiver(x,y,xdot,ydot);

'make fig nicer'
grid on;
xlabel('x ');
ylabel('y');
title('direction of $\dot{x}, \dot{y}$ dot velocity','interpreter','latex');
axis( [0.5 1. 0.5 1.]);

%figure(hFig+1);
% NOTEST