% Pendulum with constant force, study stability
%==========================================================================
% Matlab program to simulate the movement of a simple pendulum.
% The differential equations find the unknown angles of the pendulum.
% Based on code created by James Adams 31/3/14
% Updated Jan Balewski, October 2015
%==========================================================================


clear  % Clears command history
clc   % Clears command window
clf  % Clears figure window

%========= Sets initial parameters for pendulum ===========================
g = 10;  % Gravity (m/s2)
l = 1;  % pendulum length (m)
m = 3; % mass (kg)
b = 2; % dumping
u = 15;  % external force
pos0 = -pi/3;  % Initial angle 1
vel0 = -2;   % Initial anglular velocity

ml2=m*l^2;
g2l=g/l;

duration = 10;  % Runs simulations for this time

%============== Solves simple pendulum differential equations =============
deq1=@(t,x) [x(2);  -b*x(2)/ml2 + u/ml2- g2l * sin(x(1))]; % Pendulum equations uncoupled
[t,sol] = ode45(deq1,[0 duration],[pos0,vel0]);  % uses a numerical ode solver
solx1 = sol(:,1)'; % takes the transpose for plots
solx2 = sol(:,2)';

%===============  draws  xdot vs. x
figure(1);
plot(solx1,solx2,'ro')
grid on;
xlabel('\theta ');
ylabel('$\dot{\theta}$','interpreter','latex');
title('state-space for pendulum','interpreter','latex');
%axis( [0.5 1. 0.5 1.]);

%


%arraysize = size(t);  % Defines array size of time intervals
%timestep = t(runtime) - t(runtime-1);  % Calculates the time step of these intervals
%cartesianx = l*sin(sol1);  % Converts angles into cartesian coordinates
%cartesiany = l*cos(sol2);  
    

