
% Pset1-5, pendulum with constant force, study stability

clear  % Clears command history
clc   % Clears command window
clf  % Clears figure window

%========= Sets initial parameters for pendulum ===========================
g = 10;  % Gravity (m/s2)
l = 1;  % pendulum length (m)
m = 3; % mass (kg)
b = 2; % dumping
u = 15;  % external force

% range of force for which stable point exist
uL=[-29.9 :3:29.9];
thH=asin(  uL./m/g);
thL=pi-thH;
%===============  x1* vs u
figure(3);
plot(uL,thH,'ro')
hold on
plot(uL,thL,'bo')
hold off
legend('low stab','high stab')
grid on;
xlabel(' force u ');
ylabel('\theta * (rad)','interpreter','latex');
title('Bifurcation  for pendulum w/ force u');
axis( [-35 35 -2. 5.5]);
