function phasePortrait_rth(hFig)

figure(hFig);


%[Q,Qdot] = ndgrid(linspace(-3*pi/2,3*pi/2,21),linspace(-4,4,21));
[r,th] = ndgrid(linspace(0,2,21),linspace(0,2*pi,21));

'grid constructed'

rdot=r;
thdot=th;
'dims of r'
size(r)
t=0;
for i=1:prod(size(r))
  Drth=d_rth(t, [r(i) th(i)]); 
  rdot(i)=Drth(1);
  thdot(i)=Drth(2);
end

%rdot
%thdot
quiver(r,th,rdot,thdot);

'make fig nicer'
grid on;
xlabel('r ');
ylabel('\theta');
title('direction of $\dot{r}, \dot{\theta}$ dot velocity','interpreter','latex');

%figure(hFig+1);
% NOTEST