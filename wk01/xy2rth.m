function [ rth ] = xy2rth(  xy)
% Converts radial to carthesian coordinates
x=xy(1);
y=xy(2);
rth(1)=sqrt(x*x+y*y);
rth(2)=atan2(y,x);

end

