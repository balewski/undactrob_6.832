/* 
   Test Max1113_spi - quad ADC, 8-bit
   
 * Name        : test_quadCnt.cpp
 * Original Author      : Jan Balewski
 * Created on  : December 2015
 *
 * Copyright © 2015 Jan Balewski <jan.balewski@gmail.com>
*/
#include <stdio.h>
#include <unistd.h> // usleep()

#include "Max1113_spi.h"

//========================
//========================
    
class KnobData {
public:
  int swA,swB;
  int potK1, potK2,potK3;
  KnobData(){ clear();}
  void print(){
    printf("knobData switch: A=%d  B=%d   pot: K1=%d  K2=%d  K3=%d\n",swA,swB,potK1,potK2,potK3);
  }
  void clear() { swA=swB=potK1=potK2=potK3=0; }
};



//========================
//========================

KnobData readKnobData(Max1113_spi *adcChip) {
  // keep one last readout of pot-values for smooting
  static KnobData kd; // tmp, change it to call memebr variable later
  KnobData kd0=kd;

  kd.swA=1; kd.swB=1; 
  int adcChan;
  for(adcChan=0; adcChan<Max1113_spi::mxCh; adcChan++){
    int val=adcChip->read(adcChan);
    //    printf("ch=%d val=%d\n",adcChan,val);    
    switch (adcChan) {
    case 0 : {
      if (val >200 ) {
	kd.swA=0; kd.swB=0;
      } else if ( val>100 ) {
	kd.swA=0 ;
      } else if (val>48) {
	kd.swB=0;
      }
      // printf("1) swa=%d swB=%d\n",swA,swB);
    } break;
      // do average with previous value to stabilize it
    case 1: kd.potK1=(val+kd0.potK1)/2; break;
    case 2: kd.potK2=(val+kd0.potK2)/2; break;
    case 3: kd.potK3=(val+kd0.potK3)/2; break;      
    }
  }// end of channel loop

  //kd.print();
  return kd;
}



//========================
//========================

using namespace std;
int main () {
  int spiChan=1; // allowed  CE0=0, CE1=1
  printf("start Max1113 readout SPI chan=%d\n",spiChan);

  int clk_hz=500*1000; // Max1113 wants 500kHz
  Max1113_spi adcChip(spiChan,clk_hz);

  int adcChan;

  printf("quick dump of all %d ADC channals\n",Max1113_spi::mxCh);
  for(adcChan=0; adcChan<Max1113_spi::mxCh; adcChan++){
    int val=adcChip.read(adcChan);
    printf(" read(ch=%d spi=%d)  ADC=%d\n\n", adcChan, spiChan,val);
    usleep(100*1000);
  }

  KnobData knobData;
  knobData.print();

  int delayMsec=200;
  printf(" ini del=%d\n",delayMsec);
  for(int k=0; ;  k++  ){
    printf("rd k=%d ",k);
     KnobData knobData =readKnobData(&adcChip);
    knobData.print();
    //printf("  del=%d\n",delayMsec);
    //usleep( 500*1000);
    usleep(delayMsec*1000); // does not work with spiChan=1
  }

  printf("\n --- end \n");
  return 0 ;
} 
