#!/usr/bin/env python
import time
import spidev

"""
Reading 3 pots + 2 switches for pendulum controll
reading MAX1113  w/ spidev object

"""

def readAdc(adcChan, spiChan=1):
    sel2=adcChan%2
    sel1=adcChan/2
    cmd= ((2*sel2+sel1) <<5) +0b10001111 
    #print 'adcChan=%d cmd=0x%02x'%(adcChan,cmd)
    data0=0; data1=0
    cmdL=[cmd,data0,data1]
    resp = spi1.xfer2(cmdL) # transfer one byte    
    #  data format 0-0-msb-x-x-x-x-x-x-lsb-0-0-0-0-0-0
    value = (resp[1]<<2) | (resp[2]>>6)
    #print "resp1,1, 0x%02x 0x%02x"%(resp[1],resp[2])
    #print 'adc val=',value
    return value

def readPulser(txt,cmd,data0=0,data1=0):
    cmdL=[cmd,data0,data1,0xc1,0xc2,0xc3]
    resp = spi0.xfer2(cmdL) # transfer one byte
    #print txt," cmd=0x%0x=%s --> "%(cmdL[0],bin(cmdL[0]))
    sum=0
    #print "   rd-data:",
    for i in range (4):
        oneByte=resp[i+1];
        #print "0x%0X=%s  "%(resp[i+1],bin(resp[i+1])),
        sum=sum+ oneByte<<(3-i)*8
    #print ""
    return sum


delTsec=0.5

print "read start , spiChan 0+1 period/sec=",delTsec

spi0 = spidev.SpiDev() # create spi object
spi0.open(0, 0) # open spi port 0, device (CS0) 

print "spi0 bits_per_word=",spi0.bits_per_word
print "spi0 max_speed_hz=", spi0.max_speed_hz


spi1 = spidev.SpiDev() # create spi object
spi1.open(0, 1) # open spi port, device (CS0)
spi1.max_speed_hz=500000
print "spi1 bits_per_word=",spi1.bits_per_word
print "spi1 max_speed_hz=", spi1.max_speed_hz



n=0
while True:
    #....  spi 0
    cmd=0b01100111 ; txt="RD:CNTR"; 
    nPulses=readPulser(txt,cmd)
    print "n=",n,' nPules=',nPulses

    #  ..... spi 1 
    togA=1
    togB=1
    pot={}
    for ch in range(4):
        val= readAdc(ch)
        #print 'ch=',ch ,val
        if ch==0:
            if val >200:
                togA=togB=0
            elif val>100:
                togA=0
            elif val>48:
                togB=0
        else :
            pot[ch]=val

    print n,"togA=%d togB=%d"%(togA,togB),pot
    n+=1
    time.sleep(delTsec) # sleep in seconds
