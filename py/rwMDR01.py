#!/usr/bin/python
'''
writes & reads  the MDR0, MDR1 8-bit records to set up pulse-counter 

to be 4x8 bit long , and to count 1 count per quadrature cycle 

'''

def BytesToHex(Bytes):
 return ''.join(["0x%02X " % x for x in Bytes]).strip()

def doCmd(txt,cmd,data0=0,data1=0):
    cmdL=[cmd,data0,data1]
    resp = spi.xfer2(cmdL) # transfer one byte
    print txt," cmd=0x%0x=%s -->  ans=0x%0X=%s"%(cmdL[0],bin(cmdL[0]),resp[1],bin(resp[1]))
    time.sleep(0.1) # sleep for 0.1 seconds

import spidev
import time
spi = spidev.SpiDev() # create spi object
spi.open(0, 0) # open spi port 0, device (CS0) 

print "using bits_per_word=",spi.bits_per_word
print "using max_speed_hz=", spi.max_speed_hz



''' MDR0 register
  b01: 1 counts per qudrature cycle
  b10: 2 counts per qudrature cycle
  b11: 4 counts per qudrature cycle
'''
cmd=0b10001000 ; txt="WR:MDR0" ;doCmd(txt,cmd,0x2)  
'''  MDR1 register
   b00 : 4 byte depth counter
   b10 : 2 byte depth counter
'''
cmd=0b10010000 ; txt="WR:MDR1" ;doCmd(txt,cmd,0x2) 

cmd=0b01001000 ; txt="RD:MDR0"; doCmd(txt,cmd)
cmd=0b01010000 ; txt="RD:MDR1"; doCmd(txt,cmd)


spi.close(); exit(1) # close the port before exit
