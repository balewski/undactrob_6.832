#!/usr/bin/python
''' small swing  if  motor, constant  period and amplitude
'''

from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor

import time
import atexit

# create a default object, no changes to I2C address or frequency
mh = Adafruit_MotorHAT(addr=0x60)

# recommended for auto-disabling motors on shutdown!
def turnOffMotors():
	mh.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(2).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(3).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(4).run(Adafruit_MotorHAT.RELEASE)

atexit.register(turnOffMotors)

################################# DC motor test!
myMotor = mh.getMotor(1)

# set the speed to start, from 0 (off) to 255 (max speed)
myMotor.setSpeed(150)
myMotor.run(Adafruit_MotorHAT.FORWARD);
# turn on motor
myMotor.run(Adafruit_MotorHAT.RELEASE);

mxSpeed=100
periodT0=1.55  # sec, +/- 60deg
periodT=periodT0

nCyc=0
while (True):
	nCyc+=1
	print "Forward! @ full power ..., cyc=",nCyc," per=",periodT

	myMotor.run(Adafruit_MotorHAT.FORWARD)
	myMotor.setSpeed(mxSpeed)
	time.sleep(periodT/4.)

	print "sleep forw..."
	myMotor.setSpeed(0)
	time.sleep(periodT/4.)

	print "Backward! "
	myMotor.run(Adafruit_MotorHAT.BACKWARD)
	myMotor.setSpeed(mxSpeed)
	time.sleep(periodT/4.)

	print "sleep backw..."
	myMotor.setSpeed(0)
	time.sleep(periodT/4.)


#	print "Release"
#	myMotor.run(Adafruit_MotorHAT.RELEASE)
#	time.sleep(1.0)
