#!/usr/bin/python
''' 
working code to controll motor-only
'''

from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor

import time
import atexit

# create a default object, no changes to I2C address or frequency
mh = Adafruit_MotorHAT(addr=0x60)

# recommended for auto-disabling motors on shutdown!
def turnOffMotors():
	mh.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(2).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(3).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(4).run(Adafruit_MotorHAT.RELEASE)

atexit.register(turnOffMotors)

################################# DC motor test!
myMotor = mh.getMotor(1)

# set the speed to start, from 0 (off) to 255 (max speed)
myMotor.setSpeed(150)
myMotor.run(Adafruit_MotorHAT.FORWARD);
# turn on motor
myMotor.run(Adafruit_MotorHAT.RELEASE);

mxSpeed=250
kStairs=4 # division of speed steps
periodT0=1.55  # sec, +/- 60deg
periodT=periodT0


nCyc=0
dir=1
while (True):
	nCyc+=1
	timeTick=periodT/4./kStairs
	if dir > 0:
		print "Forward!  ..., cyc=",nCyc," per=",periodT," tick=",timeTick
		myMotor.run(Adafruit_MotorHAT.FORWARD)
	else :
		print "Backward! "
		myMotor.run(Adafruit_MotorHAT.BACKWARD)

        print "\tSpeed up..."
        for k in range(kStairs):
		speed=int(1.*k/kStairs*mxSpeed)
		#print "k=",k,"  speed=",speed
                myMotor.setSpeed(speed)
                time.sleep(timeTick)
	
        print "\tSpeed down..."
        for k in reversed(range(kStairs)):
		speed=int(1.*k/kStairs*mxSpeed)
		#print "k=",k,"  speed=",speed
                myMotor.setSpeed(speed)
                time.sleep(timeTick)
	
	dir=-dir

	# increase period proprotionally to # of cycles
	if nCyc >10 :
		periodT=periodT0+(nCyc -10) *.003

	

#	print "Release"
#	myMotor.run(Adafruit_MotorHAT.RELEASE)
#	time.sleep(1.0)
