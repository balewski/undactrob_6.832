#include <stdio.h>
#include "TCanvas.h"
#include "TString.h"
#include "TH1.h"
#include "TMath.h"
#include "TThread.h"
#include "TStyle.h"
#include "TText.h"
#include "TArrow.h"
#include "TLegend.h"

#include "LiveData.h"
#include "sharedObj.h"

//================================
//================================
void *handlePhasePortrait(void *ptr) {
  long dumm1=(long) ptr;
  printf("PA:start PlotterA iii=%ld\n",dumm1);
 
  TLegend* legA = new TLegend(0.7,0.9,0.99,0.99);
  legA->SetHeader("Pendulum phase image");
  legA->AddEntry(liveData3->getGr(),"vel-pos","lp");
 
  TString tit11=Form("can2");
  TString tit22=Form("phase imageA");
  TCanvas *can2 = new TCanvas(tit11,tit22,200,2,1200,700);

  printf("PA:draw Canvas\n");
  int nUpd=0; // update counter
  int nData=0;

  // units: (deg), (deg), (deg/sec) 
  //float pos1=172., pos2=189., velMx=25; //zoom-in up position
  float pos1=-200, pos2=200, velMx=600; //full free swing
  // float pos1=-50, pos2=50, velMx=480; //bottom , high speed of full free swing

  TH1F *hP2=new TH1F("bb","phase diagram --------- --------- -------- --------- --------; angle (deg); speed #Delta #phi/#Delta t (deg/sec)",100,pos1,pos2);
  hP2->SetMinimum(-velMx-5);   hP2->SetMaximum(velMx-5);

  // hP2->SetMaximum(-320); //tmp
 
  can2->cd(0);
  hP2->Draw();
  legA->Draw();
  gPad->SetGrid();
  gStyle->SetOptStat(0);

  //-------- user interface

  float myK1,myK2,myGoalAngle=goalAngle; 
  TArrow *ar4 = new TArrow(myGoalAngle,-velMx, myGoalAngle,-105,0.03,"|>");
  ar4->Draw();
 
  LiveData *ld1=0, *ld2=0, *ld3=0;   

  printf("PA: infinite loop updating plot ...\n");
  while (1) {

    if(ld3) {
      delete ld3;
      can2->Update();
    }

    if(ld2) {
      ld2->getGr()->SetMarkerColor(kGreen);
    }

    if(ld1){
      ld1->getGr()->SetMarkerSize(0.5); 
    }

    TThread::Lock(); //Static method to lock the main thread mutex.
    int np=liveData3->getN();
    //printf("P:sees %d live data\n",np);
    if (np >1 ) {
      ld3=ld2; ld2=ld1;
      ld1=liveData3; 
      liveData3=ld1->newChunk();      
      nData+=np-1;
    } 
    // update knobs values
    myK1=K1;    myK2=K2;
    // myGoalAngle=360-goalAngle; // mirror x-axis for camera
    myGoalAngle=goalAngle; 

    TThread::UnLock();


    if (ld1) {
      delete ar4;
      ar4 = new TArrow(myGoalAngle,-velMx, myGoalAngle,-15,0.05,"|>");
      ar4->SetLineWidth(4);
      ar4->SetLineColor(4);
      ar4->SetFillStyle(3008);
      ar4->SetFillColor(2);
      ar4->Draw();

      hP2->SetTitle(Form("(%d) nD=%d   K1=%.0f   K2=%.0f\n",nUpd,nData,myK1,myK2));
      nUpd++;      
      //printf("P:Update canvas time1=%d  valL=%.1f \n",time1,valL );
      ld1->getGr()->Draw("pl");
     can2->Update();
    }
    gSystem->Sleep(plotPeriod_ms);
   
  }

  
  return 0;
}

