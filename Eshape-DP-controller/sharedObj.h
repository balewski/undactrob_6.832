extern LiveData *liveData3;

extern int plotPeriod_ms;
extern long fillDuration_s;

void *handlePhasePortrait(void *ptr);
void *handlePlotterB(void *ptr);

extern float cnt2deg, deg2cnt;

extern float K1,K2,goalAngle;
extern int ctrlType;
