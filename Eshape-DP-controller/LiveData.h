#ifndef LiveData_h_
#define LiveData_h_

#include <TGraph.h>
#include <TH1.h>

class LiveData { //minimal TGraph holds chunk of fresh (not plotted) data
 private:
  TGraph *gr;
  int myCol;
 public:
  int getN(){ return gr->GetN();}
  
  void addPoint(float x, float y) {
    int np=gr->GetN();
    gr->SetPoint(np,x,y);
  }

  void clear(){
    gr->Set(1); // set size to be zero
    printf("gr clear called\n");
  }

  LiveData *newChunk() {
     double x,y;
     int np=gr->GetN();
     gr->GetPoint(np-1,x,y);
     LiveData *gr1=new LiveData(x,y);// replicate last point for continues plot-line
     gr1->setColor(myCol);
     return gr1;
  }

  TGraph *getGr() const {return gr;}
  
  ~LiveData(){
    //printf("dstr LD\n");
    delete gr;
  }

  LiveData(double x, double y) {     
    gr=new TGraph();
    gr->SetLineWidth(2);
    gr->SetMarkerStyle(4); 
    gr->SetMarkerSize(1.); 
    gr->SetPoint(0,x,y);
    setColor(2);
    // printf("cnstr LiveData\n");
  }

  void setColor(int iCol) {
    myCol=iCol;
    gr->SetLineColor(iCol);
    gr->SetMarkerColor(iCol+2);
  }

  void updateVsTime(TH1 *hT=0) {    
    gr->Draw("pl");
    //printf("hT=%p\n",hT);
    if (hT==0) return; // do not rescale y-axis
    int np=gr->GetN();
    // resacle Y-range if needed
    float yMin=hT->GetMinimum();
    float valL= TMath::MinElement(np,gr->GetY());
    if(yMin>valL) { 
      yMin=valL;
      hT->SetMinimum(yMin); 
    }    
     
    float yMax=hT->GetMaximum();
    float valH= TMath::MaxElement(np,gr->GetY());
    if(yMax<valH) {
      yMax=valH;
      hT->SetMaximum(yMax);
    }
    //printf(" rr=%f %f\n", yMin,yMax);
  }

};

#endif
