/*
The data container is chopped in to small pices (LiveData.h)
and plotter adds to display only the most recent chunk.
For each data chunk  replicate last point for continues plot-line.
Note, this code creates memory leak, because objects created by:
  liveData=new LiveData(x,y);
are never deleted

Lanuches:
-  1 thread   generating record vs. time, adding to TGraph
-  1 thread updates canvas untill joiner tells it to stop
-   main thread awaiting for filller thread to finish

*/

#include <stdio.h> 
#include <assert.h> 
#include <signal.h> // for motorHandler

#include <TMath.h>
#include <TApplication.h>
#include "TThread.h"
#include "TGraph.h"

#include "LS7336.h" // direct access to pulserCounter chip via SPI
#include "PCA9685_motorHat.h"
#include "Max1113_spi.h" // knob-box readout
#include "LiveData.h"
#include "sharedObj.h"

TThread *trdFlr=0, *trdPltA=0, *trdPltB=0;

LiveData *liveData3=0; // shared between threads

// conversion of counter pulses to deg
float cnt2deg=10., deg2cnt=1./cnt2deg;

// user interface
float K1=0.; // P-gain of DP controller
float K2=0.; // D-gain of DP controller
float K3=0.; // aux know
float goalAngle=180.; // (deg)
int ctrlType; // 1=energy shaping, 2=DP
float par_bigN360=100*360;

//----- controlling parameters
int samplePeriod_ms=20; // 33Hz sampling is enough
int plotPeriod_ms=1500;
long fillDuration_s=340;  // free fall needs 40 seconds
//int displayDuration_s=10; // length of displayed time axis 
long xyRange=350; // for phase diagram


//===================================
//===================================
float DP_ctrl(int m,  float angle, float vel, float K1, float K2, float goalAngle){
  // compute torqe and PWM driving DC motor
  float epsAngle=fmod(angle+par_bigN360,360.) - goalAngle;
  // compute torque (a.u.)
  float torque=K1*epsAngle +K2*vel;
 if(m%20==0) 
      printf("epsAngle=%.1f (deg)  vel=%.1f  torque=%.0f  goalAngle=%.1f  K1=%.0f  K2=%.2f  m=%d\n",epsAngle,vel,torque,goalAngle,K1,K2,m);
  return torque;
}

//===================================
//===================================
float Eshape_ctrl(int m,  float angDeg, float velDeg, float K1, float K2,float K3){
  float Eup=2.62+K3/10.; // [J], potential energy , up 
  float Iner=0.083; //[kg*m^2],  moment of intertia
  float bDamp=0.06; // [kg/s] , damping 

  // convert to radians
  float ang=angDeg/180.*3.1416;
  float vel=velDeg/180.*3.1416;
  
  float Ekin=Iner*vel*vel/2.;
  float Epot=Eup*(1-cos(ang))/2.;
  float Etylda=Ekin+Epot - Eup;

  float angDegEps=fmod(angDeg+par_bigN360,360.) -goalAngle;
  float angDegTol=2.;
  if( fabs(Etylda)<0.005
       && fabs(angDegEps)<angDegTol 
      && fabs(velDeg)<5.){
    printf(" FLIP  Etilda=%.3f  angDeg=%.1f\n",Etylda,angDeg);
    ctrlType=2;
    return  DP_ctrl(m, angDeg, velDeg, 62,6,goalAngle);
  }
  //printf(" angDegEps=%.1f  ets=%d\n", angDegEps,fabs(Etylda)<0.01);

  // compute torque (a.u.)
  float torque=10*K2*bDamp*vel - K1*vel*Etylda;
  //float torque= - K1*vel*Etylda;
  // if( Etylda>0) torque=0;

  if(m%10==0) 
    printf("deg: ang=%.1f vel=%.1f; J: Ekin=%.2f Epot=%.2f Etylda=%.2f   torque=%.0f  K1=%.0f  K2=%.2f K3=%.1f  m=%d\n",angDeg,velDeg,Ekin,Epot,Etylda,torque,K1,K2,K3,m);
  return torque;
}

//===================================
//===================================
void speedTrap(int m,  float angle, float vel){
  // call it to find maximal speed for each period, needed to determine
  // damping coefficient 'b', for my pendulum I got b=0.06 kg/s
 
  static bool isOn=false;
  static int nFire=0;
  if( !isOn && vel<0 && angle>0 ) {
    isOn=true;
    printf("speedTrap: enable m=%d angle=%.1f vel=%.1f \n",m,angle,vel);
  }
  if( isOn && angle<0 ) {
    nFire++;
    isOn=false;
    printf("*** speedTrap: %d fire m=%d angle=%.1f vel=%.1f \n",nFire,m,angle,vel);
  }


}


//===================================
//===================================
Adafruit_DCMotor *myMotor=0; // needed for motor-handler

void motor_emergency(sig_t s){
  TThread::Lock();
  printf("my motor emergency handler caught signal %d \n",(int)s);
  myMotor->run(RELEASE);
  myMotor=0; 
  gSystem->Sleep(2000); // mSec 

  printf("emergency stop motor done\n");
  TThread::UnLock();
  exit(1); 
}

//========================
//========================
    
class KnobData {
public:
  int swA,swB; 
  int potK1, potK2,potK3;
  KnobData(){ clear();}
  void print(){
    printf("knobData : A=%d  B=%d   pot: K1=%d  K2=%d  K3=%d\n",swA,swB,potK1,potK2,potK3);
  }
  void clear() { swA=swB=potK1=potK2=potK3=0; }
};


//========================
//========================
KnobData readKnobData(Max1113_spi *adcChip) {
  // keep one last readout of pot-values for smooting
  static KnobData kd; // tmp, change it to call memebr variable later
  KnobData kd0=kd;

  kd.swA=1; kd.swB=1; 
  int adcChan;
  for(adcChan=0; adcChan<Max1113_spi::mxCh; adcChan++){
    int val=adcChip->read(adcChan);
    //    printf("ch=%d val=%d\n",adcChan,val);    
    switch (adcChan) {
    case 0 : {
      if (val >200 ) {
	kd.swA=0; kd.swB=0;
      } else if ( val>100 ) {
	kd.swA=0 ;
      } else if (val>48) {
	kd.swB=0;
      }
      // printf("1) swa=%d swB=%d\n",swA,swB);
    } break;
      // do average with previous value to stabilize it
    case 1: kd.potK1=(val+kd0.potK1)/2; break;
    case 2: kd.potK2=(val+kd0.potK2)/2; break;
    case 3: kd.potK3=(val+kd0.potK3)/2; break;      
    }
  }// end of channel loop

  //kd.print();
  return kd;
}

 
//===================================
//===================================
void *handleFiller(void *ptr) {
  long timeOutSec = (long) ptr;
  printf("F:start Filler timeOutSec=%ld\n",timeOutSec);
  
  printf("F:DCMotor connecting ...\n");
  // Create the motor shield object with the default I2C address
  PCA9685_motorHat AFMS = PCA9685_motorHat((uint8_t)0x60);
  AFMS.begin(1600);  // create with the default frequency 1.6KHz
  // Select which 'port' M1, M2, M3 or M4. In this case, M1
  myMotor = AFMS.getMotor(1);
  assert(myMotor);

  int clk_hz=900*1024;
  int spiChan=0;
  LS7336 *pulseCntr=new LS7336 (spiChan,clk_hz);
  pulseCntr->setCounterDepth(LS7336::cycleX4, LS7336::twoByte);
  pulseCntr->readRegisters(); // for x-check
  pulseCntr->reset();  // force this cntr offset regardless of current position (rarely used)
  pulseCntr->debug=0;
  
  printf("F:ZERO counter =%d  of %d cnt\n",pulseCntr->getCntrOffset(),pulseCntr->maxCntr() );
  
  //----- knob-box reader
  spiChan=1;
  Max1113_spi adcChip(spiChan,clk_hz);

  int adcChan;
  printf("quick dump of all %d ADC channals\n",Max1113_spi::mxCh);
  for(adcChan=0; adcChan<Max1113_spi::mxCh; adcChan++){
    int val=adcChip.read(adcChan);
    printf(" read(ch=%d spi=%d)  ADC=%d\n\n", adcChan, spiChan,val);
    usleep(100*1000);
  }
 
  KnobData knobData= readKnobData(&adcChip);
  knobData.print();
  float potK1off=120; // adu
  float potK2off=120;
  float potK3off=120;
  
  int isActive=0;
  int time0=time(0);
  int nowTime=time0;
  int m=0; // DP iteration counter 
  
  // predefine motor state
  int motorDir=0; // valid dir=+1 or -1
  int motorPwm=0; // valid speed 0-250

  printf("F: read pulseCounter data  & drive DCmotor... \n");
  double lastTime=0,lastAngle=0,lastVel=0;
  double ww=0.3;
  double goalAngleLow=176,goalAngleHigh=184; // limit stability range 
  //  double angleTilt=0;  // (deg) from knob K3 
  ctrlType=1; // energy shaping

  // ....mixed-controller start....
  while ( nowTime <time0+timeOutSec) {
    double curTime=pulseCntr->procTime(); // (sec)
    double curAngle=pulseCntr->position()/cnt2deg; // (deg)
    double delT=curTime-lastTime;
    double newVel=0;
    if (delT>0) newVel=(curAngle-lastAngle)/delT;
    double curVel=(1.-ww)*lastVel +ww*newVel;
    // current angle and velocity are measured & computed
    lastTime=curTime;
    lastAngle=curAngle;
    lastVel=newVel;
    m++;
    
    // update controll params from user interface
    if(m%10==0) { 
      knobData= readKnobData(&adcChip);
      K3= 0.1*(knobData.potK3-potK3off);//range [ -12,+12]
      //angleTilt=K3; // deg, from knob K3 
      //off goalAngle=180. + angleTilt; // user set angle
      // protect stability
      if (goalAngle < goalAngleLow) goalAngle =goalAngleLow;
      if (goalAngle > goalAngleHigh) goalAngle =goalAngleHigh;
      K1=200.*(knobData.potK1/potK1off-1); // knob swing [-1,+1]
      K2=20.*(knobData.potK2/potK2off -1.); // knob swing [-1,+1]
    }

    float torque=0;
    if(ctrlType==1) torque= Eshape_ctrl(m, curAngle, newVel, -75,K2,K3);
    else torque= DP_ctrl(m, curAngle, newVel, 62,6,goalAngle);
    // speedTrap(m, curAngle, newVel);



    int newMotorDir=1;
    if (torque<0) newMotorDir=-1;
    if (newMotorDir== motorDir ) {// no change of direction
      motorPwm=(int)fabs(torque);
      if(motorPwm>250) motorPwm=250; // cap PWM range
      if( knobData.swA) myMotor->setSpeed(motorPwm); // modify speed
    } else { // change of direction
      motorDir=newMotorDir;
      motorPwm=0;
      if( knobData.swA) {
	myMotor->setSpeed(motorPwm); // stop motor before changing  direction
	if(motorDir==1) myMotor->run(FORWARD);
	if(motorDir==-1)myMotor->run(BACKWARD);
      }
    }


    if( knobData.swA)  isActive=1; // on/off of the loop
    if( isActive && !knobData.swA) {
      myMotor->run(RELEASE);
      printf("motor released by swB and quit DP\n");
      break;
   }
   

    //.... PD-controller end

    TThread::Lock();     
    //    liveData3->addPoint(360-curAngle,curVel); // mirror x-axis for camera
    liveData3->addPoint(curAngle,curVel); 
    TThread::UnLock();

    gSystem->Sleep(samplePeriod_ms); // mSec 
    nowTime=time(0);
  }
  printf("F: done, \n");
  myMotor->run(RELEASE);
  return 0;
}


//================================
//================================
void *handleCloser(void *ptr) {
  long yMax=(long) ptr;
  printf("C:start closer  inp=%ld\n",yMax);
 
   TThread::Ps();
   printf("C:wait for fill-thread to finish...\n");

   if(trdFlr) {
     trdFlr->Join(); // wait untill thread is cancelled
     printf("C:fill-done, wait for plt-thr to finish ...\n");
     TThread::Ps();
     delete trdFlr; 
   }

   if(trdPltA) {
     trdPltA->Join(); 
     printf("C:plotA-done ...\n");
     TThread::Ps();
     delete trdPltA; 
   }

   if(trdPltB) {
     trdPltB->Join(); 
     printf("C:plotB-done ...\n");
     TThread::Ps();
     delete trdPltB; 
   }
   printf("C:stopped all threads\n");
  return 0; 
}


//============================================
//============================================
  
int main(int argc, char* argv[]) {

  printf("my pendulum DP-controller   START...\n");
  
  //gDebug = 1; // prints what is load to Root
  
  TApplication app ("app",&argc,argv);
  
  // initialize data container for the 1st time

  liveData3=new LiveData(0,0);
  liveData3->setColor(2);

  //gDebug = 1; // prints what is load to Root

  //-----  DC motor driver w/ emergency handler
  signal (SIGINT,(__sighandler_t)motor_emergency);
  printf("Note, you can press ctrl-c to kill it and motor will by truned off safely by the motor emergency handler\n");

  printf("M:Starting Thread ...\n");
  //  while(1);

#if 1 // phase-plot
  trdPltA = new TThread("plPhasePort", handlePhasePortrait, (void*)  77);
  trdPltA->Run();
#endif

#if 0 // var vs. time plot
  trdPltB = new TThread("plotterB", handlePlotterB, (void*)  77);
  trdPltB->Run();  
#endif

  trdFlr = new TThread("filler", handleFiller, (void*) fillDuration_s); 
  trdFlr->Run();  
  
  printf("M: all threads are  dispatched\n");
  gSystem->Sleep(2*1000); // sometime code crashes instantly - may be this will help?

  handleCloser((void*)999); // will get stuck here untill all threds finish
  
  
  printf("M: all threads are terminated, quit Root w/ .q\n");  
  
  
  return 0;
}

