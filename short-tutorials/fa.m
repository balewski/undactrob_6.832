function [ z ] = fa( t )
%   computes parabola
%   
global a

z = (1/2)*a*t.^2 + 100;

end

