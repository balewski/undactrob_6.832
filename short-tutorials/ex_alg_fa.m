fprintf('algebraic ops on function fa, see firts what is defined\n')
global a
a=-3 % assign value needed by function 'fa'
whos
what
fprintf('find 2 zeros of parabola, assign them to c,d\n')
c=fzero(@fa,-3);  % provide fun name and starting value
d=fzero(@fa,3);
fprintf('found zeros  c=%f  d=%f\n',c,d);

A=quadl(@fa,c,d);
fprintf('integrate fa numerically between zeros, area A=%f\n  now play with default print format\n',A)

A

format long
A

format short


fprintf('define a parabola only in terms of coef, counting  p1*t^2 + p2*t + p3\n');
p=[a/2, 0,100]

rr=roots(p);

c1=min(rr)
d1=max(rr)
fprintf('found two zeros  again c1=%f  d1=%f\n  now find coefs of the integral polinom \n',c,d);

PI=polyint(p)

% here is how you can break the line by adding '...'
A1=polyval(PI,d1) - ...
    polyval(PI,c1);

fprintf('analyticaly computed integral A1=%f\n  next define t-vector to plot function and imaginary roots',A1);
p(3)=-100 % change offset of parabola so zerso do not exist
tv=[-5 : 0.1 : 5];
zv=polyval(p,tv);
plot(tv,zv);
rrIm=roots(p)

