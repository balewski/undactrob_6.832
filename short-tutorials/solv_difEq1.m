% soliving ODE  : 1st order ordinary diff equation
% the R-C circuit
clear all
global R C qf  % qf is needed by odeEvent -fucntion

R=100 %  Ohm
C=1e-3; % 1uF

% initial charge
q0=-1e-3  % init charge is  -1uC

f_qdot(0.,q0)

% expect answer : 10 mA

Tspan=[0 :0.01: 0.5];  % time grid to compute for solution

% define condition to terminate integration at Qf
qf=q0/100.;
options = odeset('events', @ odeEve);
% event information is stored in : te,qe,icon - index of condition

% the call below is w/ interuption
%[t,q] = ode45( @f_qdot, Tspan,q0); % no interuption of integral
[t,q,te,qe,icon] = ode45( @f_qdot, Tspan,q0,options);

% this is the last value of t, q in our solution
format long
[t(end)  q(end)]

[te,qe,icon]

plot(t,q,'r-');
grid on
xlabel('time (sec)')
ylabel('charge (C), eraly stop')

% do logy-plot
%semilogy(t,q,'b-');
