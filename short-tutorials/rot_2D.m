clear all
% rotate 2D points on the using eignevalues 

%define input x-y pairs

T=[0 2 4]; T=T'; % one way of entering a column
Y=[100; 80; 20]; % alternative enetering of a column vector

fprintf('this are input x-y pairs\n');
[T Y]

plot(T,Y,'ro-');
xlabel('x-val') %??? does not work
ylabel('y-val')
% define rotation angle in rad

q=45 * (pi/180.)  %rad

% define rot matrix

R=[ cos(q) -sin(q) 
    sin(q) cos(q) ]

% technuque 1 
TY= [T Y]';

TY2=(R*TY)'

% extract new T, Y
T2 = TY2(:,1);
Y2 = TY2(:,2);


hold on
plot(T2,Y2,'bo-')
axis([-110 110 -110 110])

hold off
