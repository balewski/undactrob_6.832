clear all
global a
a = -10;
t = [-5 : 0.01 : 5];
z = fa(t);

plot(t, z, 'r--')
hold on
plot(t, -z, 'b-')
hold off
legend('z1', '-z22')
title('Position vs Time')
xlabel('Time')
ylabel('Position')