'rotation matrix w/ symbolic angle q'
clear all
syms q

M = [cos(q)  -sin(q)
    sin(q) cos(q) ]
'new matrix is M^6'
M6=M^6;
M6=simplify(M6)
    

'map q to new variable pi'
syms pi
q=pi/4

'define 3 pounts and perform tjeir rotation'


'reevaluate M, M6 in terms of new symbol pi'
M=subs(M)
M6=subs(M6)

' display M  w/ 20 sig-fig precision'
vpa(M,20)

' the double [recision has only 16 sig-figs'
syms t
T=[0; 2; t]
Y=[100; 80; 20];    

% buld one matrix out of this
TY=[ T.'; Y.']  % imortant to add '.' so no conjugation is used

'rotated TY points'
TY6=simplify(M*TY)
'simplified and rounded up'
vpa(TY6,3)
