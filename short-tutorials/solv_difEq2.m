% soliving ??  : 2nd order ordinary diff equation with force
% the L-R-C circuit
clear all
global L R C qf  % qf is needed by odeEvent -fucntion

L=1e-3; % Henry
R=0; %  Ohm
C=1e-3; % 1uF

% initial charge
q0=0;  % init charge in C
i0=0; % init cyrrent in A

'test if diff-eq works'
f_qddot(0.,[q0 i0] )

% expect answer : 10 mA

tspan=[0 :1/8000: 1];  % time grid to compute for solution

% the call below is w/ interuption
[t,qi] = ode45( @f_qddot, tspan, [q0 i0]); % no interuption of integral

q=qi(:,1); i=qi(:,2);
% this is the last value of t, q in our solution
format long
[t(end)  q(end-1)  i(end-1)]



'plot  i and v over time'
plot(t,i,'r-');
hold on
plot(t,f_u(t),'b-');
hold off

title('my 2nd order diff eq. LRC circuit');

axis([0.93 1. -5 5] );
grid on
xlabel('time (sec)')
ylabel('charge (C), eraly stop')


% do logy-plot
%semilogy(t,q,'b-');


'play signal on the sound card - does not work on VM'
%soundsc(f_u(t), 8000);


'improve accuracy by tithetning tolerance'
options = odeset( 'RelTol', 1e-6, 'AbsTol',1e-9, ...
    'Stats',    'on');
% the call below reqests higher accuracy
[t,qiH] = ode45( @f_qddot, tspan, [q0 i0],options); % no interuption of integral
 iH=qiH(:,2);
 
hold on
plot(t,iH,'g-');
hold off
legend ('i(t)', 'f_u(t)','iH(t)'); 