function [ Dqi ] = f_qddot( t ,qi)
% 2st order diff eq
% is converted to a set of 2 1st order diff eq
% qi is vector, qddot is vector
global L R C

% unpack input vector
q= qi(1);

% set of diff eq
qdot=qi(2);

qddot= (-R * qdot - q/C + f_u(t) ) /L;

% repack the output , make it a column
Dqi = [ qdot qddot ]';

end

