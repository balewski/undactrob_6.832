clear all
% solve 3x3 linear equations

%define input x-y pairs

T=[0 2 4]; T=T'; % one way of entering a column
Y=[100; 80; 20]; % alternative enetering of a column vector

fprintf('this are input x-y pairs\n');
[T Y]

plot(T,Y,'ro-')
% we want to solve equation Y=A*c

% define A(T)
A=[T.^2 T ones(3,1)]

% exec for other matrix ops: helpwin elmat

Ainv=A^(-1)

%sol
c1=Ainv*Y

% chekc1

A*c1-Y

% using Gaussian elimination:
c2=A\Y

% now plot solution over input
% firts prepare input/output vectors

t=[-2:0.1:6];
y=polyval(c2,t);

hold on
plot(t,y,'b-')
hold off
grid on