function [ value isterminal direction ] ...
    = odeEve( t, q  )
% terminates integration by ode45 if terminal events occures 
global qf  % passes terminal condition

value(1) = q - qf; % want to stop at 1% of initial charge

isterminal(1)=1;  % raises exception
direction(1)=+1;  % only of q increased, if you do not care sat it to 0

end

