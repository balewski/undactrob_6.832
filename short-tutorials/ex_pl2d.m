clear all

[t,a]  = meshgrid([-5 : 0.5 : 5],...
    [-10 :0.5:0] );


plot(t, a, 'r+')

z=fb(t,a);
hold on
plot3(t,a,z,'bo');

xlable('my X-axis')
ylable('my Y-axis')
zlable('my Z-axis')
hold off