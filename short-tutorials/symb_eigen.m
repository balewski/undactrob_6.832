'eigen values of matrix w/ symbolic angle q'
clear all
syms q

M = [cos(q)  -sin(q)
    sin(q) cos(q) ];
'new matrix is M^6'
M6=M^6;
M6=simplify(M6)

syms pi
q=pi/4; M=subs(M); M6=subs(M6);

'eigen values'
lambda=simplify(eig(M))

[S,Lambda]=eig(M) % L.. has eigen values at the diagonal

