fprintf('savig 2xN matrix in to a file\n  t is transposed')

% define 1-column vector by transposing a row vector
t=[-5 :3 :5];
t=t' % this is transposition

% compute 2nd vector using func fa
global a
a=-4;
y=fa(t)

% differnt forms of combining 2 vectors
fprintf(' 2xn array:');
M1=[t y]

fprintf(' 1x2n array:');
M2=[t; y]

% open file to write
fName='data.txt'


save(fName,'M1','-ascii')
fprintf('saved file=%s, you can open it by typeing\n edit %s\n',fName, fName);
