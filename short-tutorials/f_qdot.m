function [ qdot ] = f_qdot( t ,q)
% 1st order diff eq
global R C
qdot= -1/(R*C) *q;

end

