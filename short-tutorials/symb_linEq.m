'symbolic algebra - linear eq'

syms t % this variable will caried thrugh calculation
T=[0; 2; t]
Y=[100; 80; 20];
A=[T.^2 T.^1 T.^0]

'solve lin eq by inversion'
Ainv=A^(-1)
C=Ainv*Y;
C=simplify(C)
pretty(C)
'substitute t ->4'
subs(C,t,4)

'solve by  LEFT-DIVIDE  operator'
C=A\Y;
pretty(simplify(C))